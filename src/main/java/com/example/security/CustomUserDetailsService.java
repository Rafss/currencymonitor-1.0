package com.example.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.domain.MonitorCurrency;
import com.example.domain.User;
import com.example.repository.UserRepository;
import com.example.repository.UserRole2Repository;



@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{
	private final UserRepository user2Repository;
	private final UserRole2Repository userRole2Repository;
	
	@Autowired
    public CustomUserDetailsService(UserRepository user2Repository,UserRole2Repository userRole2Repository) {
        this.user2Repository = user2Repository;
        this.userRole2Repository=userRole2Repository;
    }
	
        
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//for(User2 user:user2Repository.findAll()){
		User user=user2Repository.findByUserName(username);
		List<MonitorCurrency> mocs=new ArrayList<>();
		mocs=user.getCurrencies();
//		System.out.println(user);
		if(null == user){
			throw new UsernameNotFoundException("No user present with username: "+username);
		}else{
			List<String> userRoles=userRole2Repository.findRoleByUserName(username);
			return new CustomUserDetails(user,userRoles);
		}
	}
		
}
