package com.example.service;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.data.domain.Pageable;

import com.example.domain.Currency;

public interface CurrencyService {
	public List<Currency> getAllInCurrentDate();
	
	public List<Currency> getCurrenciesFromLastWeekByName();
	
	public void save(Currency currency);
	
	public List<Currency> findOne();
	
	public List<Currency> getCurrenciesByName();
	
	public List<Currency> findCurrenciesByNameAndPublishDateFromLast7Days(String name,int days);
}
