package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.example.domain.User;

@Service
public class EmialServiceImpl {

		private JavaMailSender javaMailSender;
		
		@Autowired
		public EmialServiceImpl(JavaMailSender javaMailSender){
			this.javaMailSender=javaMailSender;
		}
		
		public void sendEmail(User user,List<String> messages) throws MailException{
			SimpleMailMessage mail = new SimpleMailMessage();
			
			String sendTo = user.getEmail();
			
			mail.setTo(user.getEmail());
			mail.setFrom("currency.monitor.service@gmail.com");
			mail.setSubject("CurrenyMonitorSerwis powiadomienie");
			mail.setText(messages.get(0)+"\n"+messages.get(1));
			
			javaMailSender.send(mail);
		}
}
