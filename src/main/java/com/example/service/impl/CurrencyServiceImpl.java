package com.example.service.impl;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.domain.Currency;
import com.example.repository.CurrencyRepository;
import com.example.service.CurrencyService;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Autowired
	CurrencyRepository currencyRepository;

	@Override
	public List<Currency> getAllInCurrentDate() {
		Pageable topOne = new PageRequest(0, 13);
		return currencyRepository.getAllInCurrencDate(topOne);
	}

	@Override
	public void save(Currency currency) {
		currencyRepository.save(currency);

	}

	@Override
	public List<Currency> findOne() {
		Pageable topOne = new PageRequest(0, 1);
		return currencyRepository.findOne(topOne);
	}
	
	@Override
	public List<Currency> getCurrenciesFromLastWeekByName() {
		List<Currency> currencies = currencyRepository.findByNazwa("dolar amerykański");
		return currencies;
	}

	@Override
	public List<Currency> getCurrenciesByName() {
		List<Currency> currencies = currencyRepository.findByNazwa("dolar amerykański");
		return currencies;
	}

	@Override
	public List<Currency> findCurrenciesByNameAndPublishDateFromLast7Days(String name,int days) {
		DateTime dateTimeEnd = DateTime.now();
		DateTime dateTimeStart = DateTime.now().minusDays(days);
		List<Currency> currencies = currencyRepository.findByNazwaAndDataPublikacjiBetween(name,
				dateTimeStart.toDate(), dateTimeEnd.toDate());
		return currencies;
	}

}
