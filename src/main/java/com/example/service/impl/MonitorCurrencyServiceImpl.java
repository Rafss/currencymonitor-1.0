package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.domain.MonitorCurrency;
import com.example.repository.MonitorCurrencyRepository;

import com.example.service.MonitorCurrencyService;
@Service
public class MonitorCurrencyServiceImpl  implements MonitorCurrencyService{

	@Autowired
	MonitorCurrencyRepository monitorCurrencyRepository;
	
	@Override
	public List<MonitorCurrency> getAllMonitorCurrencyByUserId(Long userId) {
		return null;	
		//List<MonitorCurrency3> monitorCurrencies = monitorCurrencyRepository.findAllByUserUserId(userId);
		//return monitorCurrencies;
	}

	@Override
	public void add(MonitorCurrency monitorCurrency) {
		monitorCurrencyRepository.save(monitorCurrency);
	}

	@Override
	public void delete(long monitorCurrencyId) {
		monitorCurrencyRepository.delete(monitorCurrencyId);
	}

	@Override
	public List<MonitorCurrency> getAllMonitorCurrenciesAndUsers(List<Long> userIds) {
		return null;
		//return monitorCurrencyRepository.findAllByUserIds(userIds);
	}

}
