package com.example.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.domain.Country;

public interface CountryService {

	public List<Country> findAll();
}
