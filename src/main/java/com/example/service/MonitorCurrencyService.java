package com.example.service;

import java.util.List;

import com.example.domain.MonitorCurrency;

public interface MonitorCurrencyService {
	
	public List<MonitorCurrency> getAllMonitorCurrencyByUserId(Long userId);

	public void add(MonitorCurrency monitorCurrency);

	public void delete(long monitorCurrencyId);

	public List<MonitorCurrency> getAllMonitorCurrenciesAndUsers(List<Long> userIds);
}
