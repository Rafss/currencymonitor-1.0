package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.domain.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
   
	public List<Country> findAll();
    
}
