package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.domain.UserRole2;

@Repository
public interface UserRole2Repository extends JpaRepository<UserRole2, Long> {
	
	@Query("select a.role from UserRole2 a, User b where b.userName=?1 and a.userid=b.userId")
    public List<String> findRoleByUserName(String username);
	
}
