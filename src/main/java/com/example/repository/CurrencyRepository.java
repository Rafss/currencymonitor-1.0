package com.example.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.domain.Currency;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {

	@Query("SELECT c FROM Currency c  where c.dataPublikacji = CURRENT_DATE")
	public List<Currency> findOne(Pageable pageable);

	@Query("SELECT c FROM Currency c  ORDER BY c.dataPublikacji desc")
	public List<Currency> getCurrenciesFromLastNote(Pageable pageable);

	@Query("SELECT c FROM Currency c  ORDER BY c.dataPublikacji DESC,c.nazwa")
	public List<Currency> getAllInCurrencDate(Pageable pageable);

	@Query("SELECT c FROM Currency c  where c.dataPublikacji = CURRENT_DATE")
	public List<Currency> getAllInCurrencDateInTimestamp();

	public Currency findByid(Long id);

	public List<Currency> findByNazwa(String nazwa);

	@Query("SELECT c FROM Currency c WHERE c.nazwa=?1 AND c.dataPublikacji BETWEEN ?2 AND ?3")
	public List<Currency> findByNazwaAndDataPublikacjiBetween(String nazwa, Date start, Date end);

}
