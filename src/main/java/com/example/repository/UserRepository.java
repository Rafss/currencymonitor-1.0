package com.example.repository;

import java.util.List;

import javax.persistence.Column;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.domain.MonitorCurrency;
import com.example.domain.User;
/**
 * metody wbudwane robic przed findBy, metody własne przez getBy
 * @author rafal_000
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {
	
	public User findByUserId(Long userId);
	
	public User findByUserName(String username);
	
	public User findByEmail(String email);
	
	
    @Query("SELECT u.currencies FROM User u")
    public List<MonitorCurrency> getAllMonitorCurrenciesUser();
    
   
}
