package com.example.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "um_user_t")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long userId;

	@Column(name = "USERNAME")
	private String userName;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "ENABLED")
	private int enabled;

	@ManyToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinTable(name = "um_user_currency_t",
	joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID") 
	,inverseJoinColumns = @JoinColumn(name = "CURRENCY_MONITOR_ID", referencedColumnName = "id") )
	List<MonitorCurrency> currencies;
	
	public User() {
		super();
	}

	public User(String userName, String password, String email, int enabled,
			List<MonitorCurrency> currencies) {
		super();

		this.userName = userName;
		this.password = password;
		this.email = email;
		this.enabled = enabled;
		this.currencies = currencies;
	}

	public User(User user) {
        this.userId = user.userId;
        this.userName = user.userName;
        this.email = user.email;       
        this.password = user.password;
        this.enabled=user.enabled;
        this.currencies=currencies;
}
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getEnabled() {
		return enabled;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public List<MonitorCurrency> getCurrencies() {
		return currencies;
	}

	public void setCurrencies(List<MonitorCurrency> currencies) {
		this.currencies = currencies;
	}

	@Override
	public String toString() {
		return "User2 [userId=" + userId + ", userName=" + userName + ", password=" + password + ", email=" + email
				+ ", enabled=" + enabled + ", currencies=" + currencies + "]";
	}

}
