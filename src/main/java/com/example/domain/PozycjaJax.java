package com.example.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

//@XmlType(propOrder = { "nazwa_waluty", "przelicznik", "kod_waluty", "kurs_kupna","kurs_sprzedazy"})
@XmlRootElement(name = "pozycja")
public class PozycjaJax {

	private String nazwa_waluty;
	private int przelicznik;
	private String kod_waluty;
	private String kurs_kupna;
	private String kurs_sprzedazy;
	
	
	public String getNazwa_waluty() {
		return nazwa_waluty;
	}
	
	@XmlElement(name = "nazwa_waluty")
	public void setNazwa_waluty(String nazwa_waluty) {
		this.nazwa_waluty = nazwa_waluty;
	}
	
	public int getPrzelicznik() {
		return przelicznik;
	}
	public void setPrzelicznik(int przelicznik) {
		this.przelicznik = przelicznik;
	}
	
	@XmlElement(name = "kod_waluty")
	public String getKod_waluty() {
		return kod_waluty;
	}
	
	public void setKod_waluty(String kod_waluty) {
		this.kod_waluty = kod_waluty;
	}
	
	@XmlElement(name = "kurs_kupna")
	public String getKurs_kupna() {
		return kurs_kupna;
	}
	
	public void setKurs_kupna(String kurs_kupna) {
		this.kurs_kupna = kurs_kupna;
	}
	
	@XmlElement(name = "kurs_sprzedazy")
	public String getKurs_sprzedazy() {
		return kurs_sprzedazy;
	}
	public void setKurs_sprzedazy(String kurs_sprzedazy) {
		this.kurs_sprzedazy = kurs_sprzedazy;
	}
	
	@Override
	public String toString() {
		return "PozycjaJax  \n [nazwa_waluty=" + nazwa_waluty + "\n, przelicznik=" + przelicznik + "\n, kod_waluty="
				+ kod_waluty + "\n, kurs_kupna=" + kurs_kupna + "\n, kurs_sprzedazy=" + kurs_sprzedazy + "]";
	}
	
}

/**
*<pozycja>
*<nazwa_waluty>dolar amerykański</nazwa_waluty>
*<przelicznik>1</przelicznik>
*<kod_waluty>USD</kod_waluty>
*<kurs_kupna>3,9123</kurs_kupna>
*<kurs_sprzedazy>3,9913</kurs_sprzedazy>
*</pozycja>
**/