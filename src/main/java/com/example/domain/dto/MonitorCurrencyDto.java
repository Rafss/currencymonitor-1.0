package com.example.domain.dto;

public class MonitorCurrencyDto {

	private String name;
	private double purchase;
	private double sell;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPurchase() {
		return purchase;
	}
	public void setPurchase(double purchase) {
		this.purchase = purchase;
	}
	public double getSell() {
		return sell;
	}
	public void setSell(double sell) {
		this.sell = sell;
	}
	@Override
	public String toString() {
		return "MonitorCurrencyDto [name=" + name + ", purchase=" + purchase + ", sell=" + sell + "]";
	}
	
	
}
