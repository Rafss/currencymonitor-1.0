package com.example.domain;

import javax.persistence.*;

@Entity
@Table(name="um_user_role_t")
public class UserRole2 {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)    
    @Column(name="ID")
	private Long userroleid;
	
	@Column(name="USER_ID")
	private Long userid;
	
	@Column(name="ROLE")
	private String role;	

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getUserroleid() {
		return userroleid;
	}

	public void setUserroleid(Long userroleid) {
		this.userroleid = userroleid;
	}	
	
}
