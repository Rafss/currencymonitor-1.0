package com.example.domain;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "tabela_kursow")
public class TabelaKursowTest {
	private String typ;
	private String uid;
	private  List<PozycjaJax> pozycjaJax;
	private String numerTabeli;
	private Date dataNotowania;
	private Date dataPublikacji;

	
	public String getTyp() {
		return typ;
	}
	
	@XmlAttribute( name = "typ", required = true )
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	public String getUid() {
		return uid;
	}
	
	@XmlAttribute( name = "uid", required = true )
	public void setUid(String uid) {
		this.uid = uid;
	}

	public List<PozycjaJax> getPozycjaJax() {
		return pozycjaJax;
	}

	@XmlElement(name="numer_tabeli")
	public void setNumerTabeli(String numerTabeli) {
		this.numerTabeli = numerTabeli;
	}

	public Date getDataNotowania() {
		return dataNotowania;
	}
	
	@XmlElement(name="data_notowania")
	public void setDataNotowania(Date dataNotowania) {
		this.dataNotowania = dataNotowania;
	}

	public Date getDataPublikacji() {
		return dataPublikacji;
	}

	@XmlElement(name="data_publikacji")
	public void setDataPublikacji(Date dataPublikacji) {
		this.dataPublikacji = dataPublikacji;
	}
	
	
	@XmlElement(name="pozycja")
	public void setPozycjaJax(List<PozycjaJax> pozycjaJax) {
		this.pozycjaJax = pozycjaJax;
	}

	public String getNumerTabeli() {
		return numerTabeli;
	}

	@Override
	public String toString() {
		return "TabelaKursowTest [typ=" + typ + ", uid=" + uid + ", pozycjaJax=" + pozycjaJax + ", numerTabeli="
				+ numerTabeli + ", dataNotowania=" + dataNotowania + ", dataPublikacji=" + dataPublikacji + "]";
	}

	
}
