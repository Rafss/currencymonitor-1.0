package com.example.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cm_currency_t")
public class Currency implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "ID")
	private Long id;

	@Column(name = "NAZWA")
	private String nazwa;

	@Column(name = "KOD")
	private String kod;

	@Column(name = "PRZELICZNIK")
	private int przelicznik;

	@Column(name = "KURS_SPRZEDAZY")
	private double kursSprzedaz;

	@Column(name = "KURS_KUPNO")
	private double kursKupno;

	@Column(name = "KURS_SREDNI")
	private double kursSredni;

	@Column(name = "DATA_NOTOWANIA")
	private Date dataNotowania;;

	@Column(name = "DATA_PUBLIKACJI")
	private Date dataPublikacji;

	public Date getDataNotowania() {
		return dataNotowania;
	}

	public void setDataNotowania(Date dataNotowania) {
		this.dataNotowania = dataNotowania;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public String getKod() {
		return kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}

	public double getKursSprzedaz() {
		return kursSprzedaz;
	}

	public void setKursSprzedaz(double kursSprzedaz) {
		this.kursSprzedaz = kursSprzedaz;
	}

	public double getKursKupno() {
		return kursKupno;
	}

	public void setKursKupno(double kursKupno) {
		this.kursKupno = kursKupno;
	}

	public double getKursSredni() {
		return kursSredni;
	}

	public void setKursSredni(double kursSredni) {
		this.kursSredni = kursSredni;
	}

	public int getPrzelicznik() {
		return przelicznik;
	}

	public void setPrzelicznik(int przelicznik) {
		this.przelicznik = przelicznik;
	}

	public Date getDataPublikacji() {
		return dataPublikacji;
	}

	public void setDataPublikacji(Date dataPublikacji) {
		this.dataPublikacji = dataPublikacji;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nazwa == null) ? 0 : nazwa.hashCode());
		return result;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (nazwa == null) {
			if (other.nazwa != null)
				return false;
		} else if (!nazwa.equals(other.nazwa))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Currency [id=" + id + ", nazwa=" + nazwa + ", kod=" + kod + ", przelicznik=" + przelicznik
				+ ", kursSprzedaz=" + kursSprzedaz + ", kursKupno=" + kursKupno + ", kursSredni=" + kursSredni
				+ ", dataNotowania=" + dataNotowania + ", dataPublikacji=" + dataPublikacji + "]";
	}

}
