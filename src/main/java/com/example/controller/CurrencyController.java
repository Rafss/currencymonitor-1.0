package com.example.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.bean.CurrencyBean;
import com.example.domain.Currency;
import com.example.domain.MonitorCurrency;
import com.example.domain.User;
import com.example.repository.CurrencyRepository;
import com.example.repository.MonitorCurrencyRepository;
import com.example.repository.UserRepository;
import com.example.service.CurrencyService;
import com.example.service.MonitorCurrencyService;
import com.example.service.impl.EmialServiceImpl;


@RestController
@RequestMapping("/")
public class CurrencyController {

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private CurrencyRepository currencyRepository;

	@Autowired
	private CurrencyBean currencyBean;

	@Autowired
	private MonitorCurrencyService monitorCurrency3Service;

	@Autowired
	MonitorCurrencyRepository monitorCurrencyRepository;

	@Autowired
	UserRepository userRepository;

	@Autowired
	private EmialServiceImpl emialServiceImpl;

	@RequestMapping("/download")
	public void createObjectFromXsd() {
		currencyBean.saveCurrency();

	}

	@RequestMapping(value = "/sendEmails", method = RequestMethod.GET)
	public ResponseEntity<String> sendEmails() {
		
		Pageable pageable = new PageRequest(0,13);
		List<Currency> currencies = currencyRepository.getCurrenciesFromLastNote(pageable);
		List<String> currenciesString = new ArrayList<>();
		List<User> users = userRepository.findAll();
		List<MonitorCurrency> monitorCurrencies;
		List<String> messages = new ArrayList<String>();
		
		Map<String,Currency> map = new HashMap<>();
		for(Currency currency: currencies) {
			map.put(currency.getNazwa(), currency);
		}
		
		for(Currency currenncy:currencies) {
			currenciesString.add(currenncy.getNazwa());
		}
		
		for (User user: users) {
			String message; 
			monitorCurrencies = user.getCurrencies();
			for(MonitorCurrency monitorCurrency: monitorCurrencies) {
				if(currenciesString.contains(monitorCurrency.getName())){
					Currency currency = map.get(monitorCurrency.getName());
					if(currency.getKursKupno()>monitorCurrency.getPurchase()){//4.3779
						message = ("-Waluta " +currency.getNazwa()+ " przekroczyła twój kurs kupna("+monitorCurrency.getPurchase()+"zł)"
								+ ". Obecnie kurs kupna waluty wynosi "+currency.getKursKupno()+"zł");
						messages.add(message);
					}
					if(currency.getKursSprzedaz()<monitorCurrency.getSell()){//4.4663
						
						message = ("-Kurs sprzedaży waluty " +currency.getNazwa()+ " spadł poniżej twoich założeń("+monitorCurrency.getSell()+"zł)"
								+ ". Obecnie kurs sprzedaży waluty wynosi "+currency.getKursSprzedaz()+"zł");
						messages.add(message);
					}
				
				}
			}
			emialServiceImpl.sendEmail(user,messages);
		}
		return null;
	}

	@RequestMapping("/getAllCurrenciesFromLastNoteDay")
	public List<com.example.domain.Currency> getAll() {
		List<Currency> cc = currencyService.getAllInCurrentDate();
		return cc;
	}

	@RequestMapping("/getAllCurrenciesFromLastNoteDay/{category}")
	public List<Currency> getProductsByCategory(@PathVariable("category") String productCategory) {
		return currencyService.findCurrenciesByNameAndPublishDateFromLast7Days(productCategory, 7);
	}

	@RequestMapping("/getAllCurrenciesFromLastNoteDay/{category}/2T")
	public List<Currency> getProductsByCategory2T(@PathVariable("category") String productCategory) {
		return currencyService.findCurrenciesByNameAndPublishDateFromLast7Days(productCategory, 14);
	}

	@RequestMapping("/getAllCurrenciesFromLastNoteDay/{category}/1M")
	public List<Currency> getProductsByCategory1M(@PathVariable("category") String productCategory) {
		return currencyService.findCurrenciesByNameAndPublishDateFromLast7Days(productCategory, 30);
	}

	@RequestMapping("/getAllCurrenciesFromLastNoteDay/{category}/2M")
	public List<Currency> getProductsByCategory3M(@PathVariable("category") String productCategory) {
		return currencyService.findCurrenciesByNameAndPublishDateFromLast7Days(productCategory, 60);
	}

	@RequestMapping("/getAllMonitorCurrenciesUser/{userId}")
	public List<MonitorCurrency> getAllMonitorCurrenciesUser(@PathVariable("userId") Long userId) {

		User user = userRepository.findByUserId(userId);
		List<MonitorCurrency> userCur = new ArrayList<>(user.getCurrencies());
		for(MonitorCurrency mc:userCur) { 
			mc.setUsers(null);
		}
		
		List<MonitorCurrency> list = monitorCurrencyRepository.findAll();
		
		//List<MonitorCurrency> list2 = monitorCurrencyRepository.pobierzto(userId);
		

	
		
		return userCur;

	}
}

