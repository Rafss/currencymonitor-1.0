package com.example.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.MonitorCurrency;
import com.example.domain.User;
import com.example.domain.dto.MonitorCurrencyDto;
import com.example.repository.MonitorCurrencyRepository;
import com.example.repository.UserRepository;

@RestController
public class MonitorCurrencyController {

	@Autowired
	private MonitorCurrencyRepository monitorCurrencyRepository;
	
	@Autowired
	private UserRepository user2Repository;
	
	@RequestMapping(value = "/monitorCurrency/{add}", method = RequestMethod.POST)
	public ResponseEntity<String> addMonitorCurrency(@RequestBody MonitorCurrencyDto monitorCurrencyDto,Principal principal) {
		
		MonitorCurrency monitorCurrency = new MonitorCurrency();
		monitorCurrency.setName(monitorCurrencyDto.getName());
		monitorCurrency.setPurchase(monitorCurrencyDto.getPurchase());
		monitorCurrency.setSell(monitorCurrencyDto.getSell());
		
		
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Long userId = user.getUserId();
		User userTOSave = user2Repository.findByUserId(userId);
		
		List<MonitorCurrency> obecna = userTOSave.getCurrencies();
		obecna.add(monitorCurrency);
		
		userTOSave.setCurrencies(obecna);

		user2Repository.save(userTOSave);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
	
	
	@RequestMapping(value = "/monitorCurrency/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteMonitorCurrency(@PathVariable("id") Long id) {
		
		
		System.out.println("deleted "+id);
		monitorCurrencyRepository.delete(id);
		/*System.out.println(monitorCurrencyDto);
		MonitorCurrency monitorCurrency = new MonitorCurrency();
		monitorCurrency.setName(monitorCurrencyDto.getName());
		monitorCurrency.setUpValue(monitorCurrencyDto.getUpValue());
		monitorCurrency.setDownValue(monitorCurrencyDto.getDownValue());
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		monitorCurrency.setUserid(user.getUserid());
		
		monitorCurrencyRepository.save(monitorCurrency);
		
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);*/
		return null;
	}
}
