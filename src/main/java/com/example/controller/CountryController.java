package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Country;
import com.example.service.CountryService;


@RestController
public class CountryController {

	@Autowired
	CountryService countryService;
	
	@RequestMapping("/getAllCountry")
	public List<Country> getAll(){
		List<Country> countries = countryService.findAll();
		return countries;
	}
	
}