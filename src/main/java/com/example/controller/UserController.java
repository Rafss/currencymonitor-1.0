package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.User;
import com.example.repository.UserRepository;

@RestController
public class UserController {

	
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/user/{add}", method = RequestMethod.POST)
	public ResponseEntity<String> create(@RequestBody User user) {

		String userPassword = user.getPassword();
		String generatedBCrypt = BCrypt.hashpw(userPassword, BCrypt.gensalt(12));
		//user.setPassword(generatedBCrypt);
		System.out.println(user);
	
		user.setPassword(generatedBCrypt);
		userRepository.save(user);
		HttpHeaders headers = new HttpHeaders();
		if (user.getUserName() != null) {
			return new ResponseEntity<String>(headers, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<String>(headers, HttpStatus.BAD_REQUEST);
		}
	}
}
