package com.example.bean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.domain.Currency;
import com.example.domain.PozycjaJax;
import com.example.domain.TabelaKursowTest;
import com.example.service.CurrencyService;

@Component
public class CurrencyBean {

	@Autowired
	private CurrencyService currencyService;
	
	

	public void Elo(){
		System.out.println("Elo");
	}
	
	public void saveCurrency(){
		
		testDownloadNBP();
		createObjectFromXsd();
		
	}
	

	public void testDownloadNBP() {
		DateFormat dateFormatLastTwoDigitsOfYear = new SimpleDateFormat("yy"); 
		// digits
		String lastTwoDigitsOfCurrentYear = dateFormatLastTwoDigitsOfYear.format(Calendar.getInstance().getTime());
		System.out.println("Dwie ostatnie cyfry roku = " + lastTwoDigitsOfCurrentYear);

		System.out.println("------JodaTime------");

		DateTime dateTimeNow = DateTime.now();
		DateTime jodaTimeFirstDayOfYear = new DateTime().dayOfYear().withMinimumValue().withHourOfDay(0);

		int jodaTimeNumberDay = dateTimeNow.getDayOfYear();
		int jodaTimeNumberOfLastBuisnessDayInYear = 0;
		int jodaTimeNumberOfCurrentMonth = dateTimeNow.getMonthOfYear();
		int jodaTimeNumberOfCurrentDayMonth = dateTimeNow.getDayOfMonth();
		System.out.println("JodaTime- dziś " + dateTimeNow + " numer dzisiejszego dnia = " + jodaTimeNumberDay);
		System.out.println("JodaTime- pierwszy dzień w roku = " + jodaTimeFirstDayOfYear);

		for (int i = 0; i < jodaTimeNumberDay; i++) {
			if (jodaTimeFirstDayOfYear.getDayOfWeek() != 6 && jodaTimeFirstDayOfYear.getDayOfWeek() != 7) {
				jodaTimeNumberOfLastBuisnessDayInYear = jodaTimeNumberOfLastBuisnessDayInYear + 1;
			}
			jodaTimeFirstDayOfYear = jodaTimeFirstDayOfYear.plusDays(1);
		}

		jodaTimeNumberOfLastBuisnessDayInYear = jodaTimeNumberOfLastBuisnessDayInYear - 4;
		System.out.println(
				"JodaTime- Lizba dni pracujących od początku roku do dziś = " + jodaTimeNumberOfLastBuisnessDayInYear);

		System.out.println("JodaTime- Do pobrania xml na dziś to " + "c"
				+ checkCalendarNumber(jodaTimeNumberOfLastBuisnessDayInYear) + "z" + lastTwoDigitsOfCurrentYear
				+ checkDayOrMonth(jodaTimeNumberOfCurrentMonth) + checkDayOrMonth(jodaTimeNumberOfCurrentDayMonth)
				+ ".xml");

		String string = ("c" + checkCalendarNumber(jodaTimeNumberOfLastBuisnessDayInYear) + "z"
				+ lastTwoDigitsOfCurrentYear + checkDayOrMonth(jodaTimeNumberOfCurrentMonth)
				+ checkDayOrMonth(jodaTimeNumberOfCurrentDayMonth) + ".xml");

		try {
			InputStream is = null;
			URL website = new URL("http://www.nbp.pl/kursy/xml/" + string);

			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			FileOutputStream fos = new FileOutputStream("C:/Users/rafal_000/Desktop/tabelaKursowWymienialnych.xml");
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			is = website.openStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(is, Charset.forName("ISO-8859-2")));
			fos.close();
		} catch (Exception e) {

		}

	}
	
	public void createObjectFromXsd() {
		boolean isEmpyTable=false;
		List<Currency> currencies=null;
		
		try{
			currencies = currencyService.findOne();
		}catch (Exception e){
			isEmpyTable=true;
		}
		
		DateTime todayDay=DateTime.now();
		int dayOfWeekNumber = todayDay.getDayOfWeek();
		
		if(dayOfWeekNumber == 6 || dayOfWeekNumber == 7){
			return;
		}

		try {
			if (currencies.isEmpty()) {
				System.out.println("ok");
				File file = new File("C:/Users/rafal_000/Desktop/tabelaKursowWymienialnych.xml");
				JAXBContext jaxbContext = JAXBContext.newInstance(TabelaKursowTest.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				TabelaKursowTest PozycjaJaxs = (TabelaKursowTest) jaxbUnmarshaller.unmarshal(file);

				List<PozycjaJax> currencysJax = PozycjaJaxs.getPozycjaJax();

				for (int i = 0; i < currencysJax.size(); i++) {
					Currency currency = new Currency();

					currency.setNazwa(currencysJax.get(i).getNazwa_waluty());
					currency.setPrzelicznik(currencysJax.get(i).getPrzelicznik());
					currency.setKod(currencysJax.get(i).getKod_waluty());

					String kursKup = currencysJax.get(i).getKurs_kupna().replace(",", ".");
					currency.setKursKupno(Double.valueOf(kursKup));
					String kursSprze = currencysJax.get(i).getKurs_sprzedazy().replace(",", ".");
					currency.setKursSprzedaz(Double.valueOf(kursSprze));

					currency.setDataNotowania(PozycjaJaxs.getDataNotowania());
					currency.setDataPublikacji(PozycjaJaxs.getDataPublikacji());

					currency.setKursSredni(0);

					System.out.println(currency);

					currencyService.save(currency);
				}			
			} else {
				System.out.println("nope");	
			}
		} catch (Exception e) {
			System.out.println("ERRO IN XSD " + e);
		}

	}
	
	
	public static String checkDayOrMonth(int dayOrMonth) {
		if (dayOrMonth < 10) {
			return ("0" + String.valueOf(dayOrMonth));
		} else {
			return String.valueOf(dayOrMonth);
		}
	}

	public static String checkCalendarNumber(int calendarNumber) {
		if (calendarNumber < 100) {
			return ("0" + String.valueOf(calendarNumber));
		} else {
			return String.valueOf(calendarNumber);
		}
	}

	public static String checkWorkingDayCurrent(int workingDay) {
		if (workingDay < 100) {
			return ("0" + String.valueOf(workingDay));
		} else {
			return String.valueOf(workingDay);
		}
	}
}
