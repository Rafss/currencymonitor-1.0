app.controller('createAccount',function($http,$scope,$location,$rootScope){
	
	
	var self = this;
	
	self.createAccount = function() {
		console.log("createAccount");
		console.log($scope.login+" "/*+$scope.firstName+" "+$scope.lastName+" "*/
					+$scope.email+" "+$scope.password+" "+$scope.rePassword);
	
		var dataObj = {
				userName : $scope.login,
				password : $scope.password,
				email : $scope.email,
				enabled: 1
			};
		
		var res = $http.post('/user/add.json', dataObj).then(function successCallback(response) {
		    // this callback will be called asynchronously
		    // when the response is available
			console.log("OK "+ response);
			$rootScope.accountCreated = true;
			$location.path( "/home" );
		  }, function errorCallback(response) {
			  $rootScope.accountCreated = false;
			  console.log("NO "+ response);
			  
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		  });
		
	};
	
});