/*angular.module('myApp.controllers', []);*/

var app = angular.module('hello', [ 'ngRoute','chart.js', 'ui.bootstrap']).config(function($routeProvider, $httpProvider) {

	$routeProvider.when('/', {
		templateUrl : 'home.html',
		controller : 'home',
		controllerAs: 'controller'
	}).when('/login', {
		templateUrl : 'login.html',
		controller : 'navigation',
		controllerAs: 'controller'
	}).when('/createAccount',{
		templateUrl : 'createAccount.html',
		controller : 'createAccount',
		controllerAs: 'controller'
	}).when('/configureNotifications',{
		templateUrl : 'configureNotifications.html',
		controller : 'configureNotifications',
		controllerAs: 'configureNotifications'
	}).when('/selectedCurrency',{
		templateUrl : 'currency.html',
		controller : 'selectedCurrency',
		controllerAs: 'controller'
	}).otherwise('/');
	

	$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
	
	
});