var app = angular.module('hello', [ 'ngRoute','chart.js', 'ui.bootstrap']).config(function($routeProvider, $httpProvider) {

	$routeProvider.when('/', {
		templateUrl : 'home.html',
		controller : 'home',
		controllerAs: 'controller'
	}).when('/login', {
		templateUrl : 'login.html',
		controller : 'navigation',
		controllerAs: 'controller'
	}).when('/createAccount',{
		templateUrl : 'createAccount.html',
		controller : 'createAccount',
		controllerAs: 'controller'
	}).when('/configureNotifications',{
		templateUrl : 'configureNotifications.html',
		controller : 'configureNotifications',
		controllerAs: 'configureNotifications'
	}).when('/selectedCurrency',{
		templateUrl : 'currency.html',
		controller : 'selectedCurrency',
		controllerAs: 'selectedCurrency'
	}).otherwise('/');
	

	$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
	
	
});

app.config(function(ChartJsProvider) {
	ChartJsProvider.setOptions({
		colours : [ '#141A1F', '#46BFBD', '#DCDCDC', '#46BFBD', '#FDB45C',
				'#949FB1', '#4D5360' ],
		responsive : false
	});
	ChartJsProvider.setOptions('Doughnut', {
		animateScale : true
	});
});

app.controller('navigation',

		function($rootScope, $http, $location, $route,$scope) {

			$rootScope.isLoginFailed = false;
			var self = this;
			$scope.name;
			
			self.tab = function(route) {
				return $route.current && route === $route.current.controller;
			};

			var authenticate = function(credentials, callback) {
					console.log("authenticate poszlo");
				var headers = credentials ? {
					authorization : "Basic "
							+ btoa(credentials.username + ":"
									+ credentials.password)
									
				} : {};

				$http.get('user', {
					headers : headers
				}).success(function(data) {
					if (data.name) {
						$rootScope.authenticated = true;
						//$scope.name = data.name;
						console.log(data.name);
					} else {
						$rootScope.authenticated = false;
					}
					callback && callback($rootScope.authenticated);
				}).error(function() {
					$rootScope.authenticated = false;
					callback && callback(false);
				});

			}

			authenticate();

			self.credentials = {};
			self.login = function() {
				console.log("==login==");
				authenticate(self.credentials, function(authenticated) {
					if (authenticated) {
						console.log("Login succeeded")
						$location.path("/");
						self.error = false;
						$rootScope.authenticated = true;
					} else {
						console.log("Login failed")
						$location.path("/login");
						self.error = true;
						$rootScope.authenticated = false;
						$rootScope.isLoginFailed = true;
					}
				})
			};
			self.logout = function() {
				$http.post('logout', {}).finally(function() {
					$rootScope.authenticated = false;
					$location.path("/");
				});
			}

		})
	app.controller('home', function($rootScope,$scope, $timeout, $http, $window,$location,CurrencyService) {
		
	$scope.chart;
	$scope.currencies = [];
	$scope.currenciesKupno = [];
	$scope.currenciesSprzedaz = [];
	$scope.chart;	
	$scope.powiedzZeTak='XD';	
	
	
	console.log($rootScope.accountCreated);
	
	var self = this;	
	$http.get('/resource/').success(function(data) {
		self.greeting = data;
	})
	
	$http.get('getAllCurrenciesFromLastNoteDay.json')
	.success(
			function(data) {
				console.log($scope.chart);
				$scope.currencies = data;
				for (var i = 0; i < 12; i++) {
					$scope.currenciesKupno
							.push($scope.currencies[i].kursKupno);
				}

				for (var i = 0; i < 12; i++) {
					$scope.currenciesSprzedaz
							.push($scope.currencies[i].kursSprzedaz);
				}

				for (var i = 0; i < 12; i++) {

					$scope.currencies[i].dataPublikacji = new Date(
							$scope.currencies[i].dataPublikacji);
				}
				console.log("GITARA 1");
			});
	
	$timeout(function() {

		$scope.labels = [ 'dolar amerykański', 'dolar australijski',
				'dolar kanadyjski', 'euro', 'forint (Węgry)',
				'frank szwajcarski', 'funt szterling', 'jen (Japonia)',
				'korona czeska', 'korona duńska', 'korona norweska',
				'korona szwedzka' ];

		$scope.series = [ 'Koszt kupna ', 'Koszt sprzedazy ' ];

		$scope.options = {
			scaleBeginAtZero : false,
			scaleShowGridLines : true,
			scaleGridLineWidth : 1,
			scaleShowHorizontalLines : true,
			scaleLabel : "<%= Number(value).toFixed(2).replace('.', ',') + ' zł'%>",
			barStrokeWidth : 2,
			barValueSpacing : 20,
			multiTooltipTemplate : "<%=datasetLabel+ value + ' zł' %>",
			tooltipTitleFontSize : 18,
			labelFontSize : 24

		};

		$scope.buyingRate = [ ($scope.currenciesKupno[0]),
				($scope.currenciesKupno[1]), ($scope.currenciesKupno[2]),
				($scope.currenciesKupno[3]), ($scope.currenciesKupno[4]),
				($scope.currenciesKupno[5]), ($scope.currenciesKupno[6]),
				($scope.currenciesKupno[7]), ($scope.currenciesKupno[8]),
				($scope.currenciesKupno[9]), ($scope.currenciesKupno[10]),
				($scope.currenciesKupno[11]) ];
		$scope.sellingRate = [ ($scope.currenciesSprzedaz[0]),
				($scope.currenciesSprzedaz[1]), ($scope.currenciesSprzedaz[2]),
				($scope.currenciesSprzedaz[3]), ($scope.currenciesSprzedaz[4]),
				($scope.currenciesSprzedaz[5]), ($scope.currenciesSprzedaz[6]),
				($scope.currenciesSprzedaz[7]), ($scope.currenciesSprzedaz[8]),
				($scope.currenciesSprzedaz[9]),
				($scope.currenciesSprzedaz[10]),
				($scope.currenciesSprzedaz[11]) ];

		$scope.data = [ $scope.buyingRate, $scope.sellingRate ];
	}, 200)
	
	
	  $scope.onClick = function(points, tooltip, evt) {
			var path = points[0].label;
			// var code=points[0]
			console.log(path);
			CurrencyService.setCurrency(path);
			sessionStorage.setItem('selectedCurrency', path)
			console.log(CurrencyService.getCurrency());
			$location.path( "/selectedCurrency" );
		};
	 
	
});

app.controller('createAccount',function($http,$scope,$location,$rootScope){
	
	
	var self = this;
	
	self.createAccount = function() {
		console.log("createAccount");
		console.log($scope.login+" "/*+$scope.firstName+" "+$scope.lastName+" "*/
					+$scope.email+" "+$scope.password+" "+$scope.rePassword);
	
		var dataObj = {
				userName : $scope.login,
				password : $scope.password,
				email : $scope.email,
				enabled: 1
			};
		
		var res = $http.post('/user/add.json', dataObj).then(function successCallback(response) {
			$rootScope.accountCreated = true;
			$location.path( "/home" );
		  }, function errorCallback(response) {
			  $rootScope.accountCreated = false;
		  });
		
	};
	
});

app.controller('configureNotifications',function($http,$scope,$timeout){
	console.log('configureNotifications');
	
	
	
	var self = this;
	$scope.countries; 
	$scope.userId;
	$scope.monitorCurriencies;
	$scope.monitorCurrencyId = true;
	$scope.i = 0;
	
	$http.get('getAllCountry.json').success(
			
			function(data) {
				console.log(data[1].name);
				$scope.notes = data;
			});
	
	
	$http.get('user', {}).success(function(data) {
		
		if(data.name){
			console.log("jest data= "+ data.name);
			console.log("ID="+data.principal.userId);
		}
		
		if (data.name) {
			$scope.userId=data.principal.userId;
			console.log($scope.userId);
			$http.get('/getAllMonitorCurrenciesUser/'+data.principal.userId+'.json').success(
					function(data) {
						$scope.monitorCurriencies = data;
						
					});
		} else {
			console.log('CONFIGURE NOTIFICATIONS nie ma data.name');
		}
	}).error(function() {
		console.log('CONFIGURE NOTIFICATIONS nie mozna uderzyć do /user');
	});
	
	self.addMonitorCurrency = function() {
		console.log("Add monitor currency start!");
		console.log($scope.note.name+" "+$scope.purchase+" "+$scope.sell);
	
		

		console.log("Jeszcze dziala1 "+ $scope.userId);
		var dataObj = {
				name : $scope.note.name,
				purchase : $scope.purchase,
				sell : $scope.sell
			};
		
		console.log("Jeszcze dziala2 "+ $scope.userId);
		var res = $http.post('/monitorCurrency/add.json', dataObj).then(function successCallback(response) {
			console.log("ok");
			$http.get('/getAllMonitorCurrenciesUser/'+$scope.userId+'.json').success(
					function(data) {
						$scope.monitorCurriencies = data;
					});	
		  }, function errorCallback(response) {
			 console.log("nie"); 
		  });
		
	};
	
	 self.remove = function(id){
         console.log('id to be deleted', id);
         $http.delete('/monitorCurrency/delete/'+id)
			.then(
					function(response){
						console.log('poszlo');
						
						$http.get('/getAllMonitorCurrenciesUser/'+$scope.userId+'.json').success(
								function(data) {
									$scope.monitorCurriencies = data;
								});	
					 
						
					}, 
					function(errResponse){
						console.log('nie poszlo');
						
					}
			);
       
     };
     self.remove2 = function(){
         console.log('chujnia');
       
     };
	
	
	
});

app.controller('selectedCurrency',function($http,$scope,$location,$rootScope,$filter){
	$scope.selectedCurrency = sessionStorage.getItem('selectedCurrency');
	console.log('getAllCurrenciesFromLastNoteDay/' + $scope.selectedCurrency);
	
	$scope.specifyCurrencies = [];
	$scope.currenciesKupnoSpecify = [];
	$scope.currenciesSprzedazSpecify = [];
	$scope.currenciesDates = [];
	$scope.currenciesDates2T = [];
	$scope.is3MOrHigher= 0;
	
	
	$http.get(
			'http://localhost:8080/getAllCurrenciesFromLastNoteDay/'
					+ $scope.selectedCurrency + '.json').success(
			function(data) {
				$scope.specifyCurrencies = data;

				for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
					$scope.currenciesKupnoSpecify
							.push($scope.specifyCurrencies[i].kursKupno);
				}

				for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
					$scope.currenciesSprzedazSpecify
							.push($scope.specifyCurrencies[i].kursSprzedaz);
				}
				
				for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
					$scope.currenciesDates.push(new Date($scope.specifyCurrencies[i].dataPublikacji));	
				}
				
				for (var i = 0; i < $scope.currenciesDates.length; i++) {
					$scope.currenciesDates[i]=$filter('date')($scope.currenciesDates[i],'d/M/yyyy');
				}
				
			});
	if($scope.is3MOrHigher>0){
		console.log('rowna sie wieksze od 0');
		$scope.options = {
				//scaleBeginAtZero : true,
				scaleShowGridLines : true,
				scaleGridLineWidth : 1,
				scaleShowHorizontalLines : true,
				scaleStartValue: 0, 
				scaleStepWidth: 1,
				ticksMaxRotation:30,
				ticksMinRotation:30,
				scaleSteps: 30,
				scaleLabel : "<%= Number(value).toFixed(2).replace('.', ',') + ' asdasd'%>",
				barStrokeWidth : 2,
				barValueSpacing : 20,
				/*multiTooltipTemplate : "<%=datasetLabel+ value + ' asdsad' %>",*/
				tooltipTitleFontSize : 18,
				labelFontSize : 24,
				showXLabels: false

			};
	} else {
		
		console.log('rowna sie 0');
		$scope.options = {
				//scaleBeginAtZero : true,
				scaleShowGridLines : true,
				scaleGridLineWidth : 1,
				scaleShowHorizontalLines : true,
				scaleStartValue: 0, 
				scaleStepWidth: 1,
				ticksMaxRotation:30,
				ticksMinRotation:30,
				scaleSteps: 30,
				scaleLabel : "<%= Number(value).toFixed(2).replace('.', ',') + ' zł'%>",
				barStrokeWidth : 2,
				barValueSpacing : 20,
				multiTooltipTemplate : "<%=datasetLabel+ value + ' zł' %>",
				tooltipTitleFontSize : 18,
				labelFontSize : 24,
				

			};
	}


	$scope.labels = $scope.currenciesDates ;
	$scope.series = [ 'Koszt kupna ', 'Koszt sprzedazy ' ];
	$scope.data = [ $scope.currenciesKupnoSpecify,
			$scope.currenciesSprzedazSpecify ];
	
	
	$scope.onChangeTime1T = function() {
		$http.get('http://localhost:8080/getAllCurrenciesFromLastNoteDay/'
						+ $scope.selectedCurrency + '.json').success(
				function(data) {
					$scope.specifyCurrencies.length = 0;
					$scope.currenciesKupnoSpecify.length = 0;
					$scope.currenciesSprzedazSpecify.length = 0;
					$scope.currenciesDates.length = 0;
					
					$scope.specifyCurrencies = data;

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesKupnoSpecify
							.push($scope.specifyCurrencies[i].kursKupno);
					}

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesSprzedazSpecify
							.push($scope.specifyCurrencies[i].kursSprzedaz);
					}
						
					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesDates.push(new Date($scope.specifyCurrencies[i].dataPublikacji));	
					}
						
					for (var i = 0; i < $scope.currenciesDates.length; i++) {
						$scope.currenciesDates[i]=$filter('date')($scope.currenciesDates[i],'d/M/yyyy');
					}
	
				});
	};
	
	
	$scope.onChangeTime2T = function() {
		$http.get('http://localhost:8080/getAllCurrenciesFromLastNoteDay/'
						+ $scope.selectedCurrency + '/2T.json').success(
				function(data) {
					$scope.specifyCurrencies.length = 0;
					$scope.currenciesKupnoSpecify.length = 0;
					$scope.currenciesSprzedazSpecify.length = 0;
					$scope.currenciesDates.length = 0;
					
					$scope.specifyCurrencies = data;

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesKupnoSpecify
							.push($scope.specifyCurrencies[i].kursKupno);
					}

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesSprzedazSpecify
							.push($scope.specifyCurrencies[i].kursSprzedaz);
					}
						
					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesDates.push(new Date($scope.specifyCurrencies[i].dataPublikacji));	
					}
						
					for (var i = 0; i < $scope.currenciesDates.length; i++) {
						$scope.currenciesDates[i]=$filter('date')($scope.currenciesDates[i],'d/M/yyyy');
					}
	
				});
	};
	
	$scope.onChangeTime1M = function() {
		$http.get('http://localhost:8080/getAllCurrenciesFromLastNoteDay/'
						+ $scope.selectedCurrency + '/1M.json').success(
				function(data) {
					$scope.specifyCurrencies.length = 0;
					$scope.currenciesKupnoSpecify.length = 0;
					$scope.currenciesSprzedazSpecify.length = 0;
					$scope.currenciesDates.length = 0;
					
					$scope.specifyCurrencies = data;

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesKupnoSpecify
							.push($scope.specifyCurrencies[i].kursKupno);
					}

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesSprzedazSpecify
							.push($scope.specifyCurrencies[i].kursSprzedaz);
					}
						
					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesDates.push(new Date($scope.specifyCurrencies[i].dataPublikacji));	
					}
						
					for (var i = 0; i < $scope.currenciesDates.length; i++) {
						$scope.currenciesDates[i]=$filter('date')($scope.currenciesDates[i],'d/M/yyyy');
					}
	
				});
	};
	
	$scope.onChangeTime2M = function() {
		$http.get('http://localhost:8080/getAllCurrenciesFromLastNoteDay/'
						+ $scope.selectedCurrency + '/2M.json').success(
								
				function(data) {
					$scope.is3MOrHigher=1;
					$scope.specifyCurrencies.length = 0;
					$scope.currenciesKupnoSpecify.length = 0;
					$scope.currenciesSprzedazSpecify.length = 0;
					$scope.currenciesDates.length = 0;
					
					$scope.specifyCurrencies = data;

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesKupnoSpecify
							.push($scope.specifyCurrencies[i].kursKupno);
					}

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesSprzedazSpecify
							.push($scope.specifyCurrencies[i].kursSprzedaz);
					}
						
					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesDates.push(new Date($scope.specifyCurrencies[i].dataPublikacji));	
					}
						
					for (var i = 0; i < $scope.currenciesDates.length; i++) {
						$scope.currenciesDates[i]=$filter('date')($scope.currenciesDates[i],'d/M/yyyy');
					}
	
					$scope.options = {
							//scaleBeginAtZero : true,
							scaleShowGridLines : true,
							scaleGridLineWidth : 1,
							scaleShowHorizontalLines : true,
							scaleStartValue: 0, 
							scaleStepWidth: 1,
							ticksMaxRotation:30,
							ticksMinRotation:30,
							scaleSteps: 30,
							scaleLabel : "<%= Number(value).toFixed(2).replace('.', ',') + ' zł'%>",
							barStrokeWidth : 2,
							barValueSpacing : 20,
							multiTooltipTemplate : "<%=datasetLabel+ value + ' zł' %>",
							tooltipTitleFontSize : 18,
							labelFontSize : 24,
							showXLabels: false

						};
			
					
				});
		
		
		
		
	};
	
	$scope.onChangeTime6M = function() {
		$http.get('http://localhost:8080/getAllCurrenciesFromLastNoteDay/'
						+ $scope.selectedCurrency + '/6M.json').success(
				function(data) {
					$scope.specifyCurrencies.length = 0;
					$scope.currenciesKupnoSpecify.length = 0;
					$scope.currenciesSprzedazSpecify.length = 0;
					$scope.currenciesDates.length = 0;
					
					$scope.specifyCurrencies = data;

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesKupnoSpecify
							.push($scope.specifyCurrencies[i].kursKupno);
					}

					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesSprzedazSpecify
							.push($scope.specifyCurrencies[i].kursSprzedaz);
					}
						
					for (var i = 0; i < $scope.specifyCurrencies.length; i++) {
						$scope.currenciesDates.push(new Date($scope.specifyCurrencies[i].dataPublikacji));	
					}
						
					for (var i = 0; i < $scope.currenciesDates.length; i++) {
						$scope.currenciesDates[i]=$filter('date')($scope.currenciesDates[i],'d/M/yyyy');
					}
	
				});
	};
	
});


app.factory("CurrencyService", function() {
	var currency;
	return {
		getCurrency : function() {
			return currency;
		},
		setCurrency : function(value) {
			currency = value;
		}
	};
});

